#!/usr/bin/env python
# coding: utf-8
"""fichier de test de la classe SimpleCalculator"""

from calculator.simple_calculator import SimpleCalculator

"""
on importe les packages dans pythonpath

on importe la classe SimpleCalculator
"""
if __name__ == '__main__':

    TEST = SimpleCalculator(27, 2.7)
    print(TEST.addition())
    print(TEST.soustraction())
    print(TEST.produit())
    print(TEST.quotient())
