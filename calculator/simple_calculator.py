#!/usr/bin/env python
# coding: utf-8

"""petite calculette sympatique"""

class SimpleCalculator:     #on appelle SimpleCalculator le nom de la classe

    """on défini la classe SimpleCalculator"""

    def __init__(self, nombre_a, nombre_b):     #init de la calculatrice avec 2 nombres
        """on place les deux arguments dans le self"""
        self.nombre_a = nombre_a
        self.nombre_b = nombre_b

    def addition(self):
        """donne la somme"""
        if (isinstance(self.nombre_a, int) and isinstance(self.nombre_b, int)):
            return self.nombre_a + self.nombre_b
        return "ERROR"

    def soustraction(self):
        """donne la soustraction"""
        if (isinstance(self.nombre_a, int) and isinstance(self.nombre_b, int)):
            return self.nombre_a - self.nombre_b
        return "ERROR"


    def produit(self):
        """donne le produit"""
        if (isinstance(self.nombre_a, int) and isinstance(self.nombre_b, int)):
            return self.nombre_a * self.nombre_b
        return "ERROR"

    def quotient(self):
        """donne le quotient"""
        if (isinstance(self.nombre_a, int) and isinstance(self.nombre_b, int)):
            if self.nombre_b != 0:
                return self.nombre_a / self.nombre_b
            raise ZeroDivisionError("Cannot divide by zero")
        return "ERROR"

